// set auth header
import store from "../../../store";

import * as error_handlers from "./errorHandlers";
import { securedAxiosInstance, plainAxiosInstance } from "./constants";

securedAxiosInstance.interceptors.request.use(config => {
  config.headers = {
    ...config.headers,
    Authorization: "Bearer " + store.state.currentUser.authToken
  };
  return config;
});

// handle errors
securedAxiosInstance.interceptors.response.use(null, error => {
  error_handlers.handle_error(error);
  return Promise.reject(error);
});

plainAxiosInstance.interceptors.response.use(null, error => {
  error_handlers.handle_error(error);
  return Promise.reject(error);
});

// retry 401
securedAxiosInstance.interceptors.response.use(null, error => {
  return error_handlers.handle_unauthorized(error, plainAxiosInstance);
});
