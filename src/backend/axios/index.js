import { securedAxiosInstance, plainAxiosInstance } from "./modules/constants";
require("./modules/inteceptors.js");

export { securedAxiosInstance, plainAxiosInstance };
