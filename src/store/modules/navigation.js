const initialState = () => ({
  open: true
});

const getters = {};

const actions = {};

const mutations = {
  setNavigationOpen(state, value) {
    state.open = value;
  },
  resetState(state) {
    const initial = initialState();
    Object.keys(initial).forEach(key => {
      state[key] = initial[key];
    });
  }
};

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
};
