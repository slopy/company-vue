module.exports = {
  rules: {
    "no-console": "off",
    indent: ["error", 2],
    "linebreak-style": [2, "unix"],
    semi: [2, "always"],
    "no-param-reassign": 0
  },
  env: {
    browser: true,
    node: true,
    es6: true
  },
  parser: "babel-eslint",
  extends: "eslint:recommended"
};
