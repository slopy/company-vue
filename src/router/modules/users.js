import UsersTable from "./../../components/users/UsersTable.vue";

const usersRoutes = [
  {
    path: "/users",
    name: "UsersTable",
    component: UsersTable
  }
];

export { usersRoutes };
