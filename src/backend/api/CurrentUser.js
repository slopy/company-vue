import { securedAxiosInstance, plainAxiosInstance } from "./../axios";

export default {
  getCurrentUserDetails() {
    return securedAxiosInstance.get("/users/current").then(response => {
      return response.data;
    });
  },
  updateCurrentUserDetails(params) {
    return securedAxiosInstance.put("/users/current", params).then(response => {
      return response.data;
    });
  },
  // session
  signIn(params) {
    return plainAxiosInstance.post("/users/signin", params).then(response => {
      return response.data;
    });
  },
  signUp(params) {
    return plainAxiosInstance.post("/users/signup", params).then(response => {
      return response.data;
    });
  }
};
