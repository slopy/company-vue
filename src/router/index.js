import Vue from "vue";
import Router from "vue-router";

import TheMainPage from "./../components/pages/TheMainPage.vue";
import { currentUserRoutes } from "./modules/currentUser.js";
import { usersRoutes } from "./modules/users.js";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      component: TheMainPage
    },
    ...currentUserRoutes,
    ...usersRoutes
  ]
});
