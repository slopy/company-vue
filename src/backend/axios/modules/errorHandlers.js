import store from "../../../store";
// handle errors
export function handle_server_error(error, error_data) {
  if (error_data === undefined && error.response && error.response.status === 500) {
    return { error: "Server error", message: "Server Error" };
  } else {
    return error_data;
  }
}

export function handle_regular_error(error, error_data) {
  if (error_data === undefined && error.response) {
    return error.response.data;
  } else {
    return error_data;
  }
}

export function handle_error(error) {
  var error_data;
  error_data = handle_server_error(error, error_data);
  error_data = handle_regular_error(error, error_data);
  store.commit("notifications/setErrors", error_data);
}

export function handle_unauthorized(error, plainAxiosInstance) {
  if (error.response && error.response.config && error.response.status === 401) {
    return plainAxiosInstance
      .get("/users/current", {
        headers: { Authorization: "Bearer " + store.state.currentUser.authToken }
      })
      .then(response => {
        store.commit("currentUser/setCurrentuserDetails", response.data);
        store.commit("notifications/resetErrors");
        let retryConfig = error.response.config;
        retryConfig.headers["Authorization"] = "Bearer " + store.state.currentUser.authToken;
        return plainAxiosInstance.request(retryConfig);
      })
      .catch(error => {
        store.commit("currentUser/unsetCurrentUser");
        location.replace("/");
        return Promise.reject(error);
      });
  } else {
    return Promise.reject(error);
  }
}
