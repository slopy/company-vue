const initialState = () => ({
  errors: {
    message: null,
    messages: [],
    show: false
  },
  success: {
    message: null,
    messages: [],
    show: false
  }
});

const helpers = {
  resetState(state) {
    const initial = initialState();
    Object.keys(initial).forEach(key => {
      state[key] = initial[key];
    });
  }
};

const getters = {};

const actions = {
  clearErrors({ commit }) {
    commit("resetErrors");
  },
  clearSuccess({ commit }) {
    commit("resetSuccess");
  },
  clearState({ commit }) {
    commit("resetState");
  }
};

const mutations = {
  setErrors(state, data) {
    if (data) {
      helpers.resetState(state);
      state.errors.show = true;
      state.errors.message = data.error;
      state.errors.messages = data.messages;
    }
  },
  setSuccess(state, data) {
    if (data) {
      helpers.resetState(state);
      state.success.show = true;
      state.success.message = data.message;
      state.success.messages = data.messages;
    }
  },
  resetErrors(state) {
    const initial = initialState();
    Object.keys(initial.errors).forEach(key => {
      state[key] = initial.errors[key];
    });
  },
  resetSuccess(state) {
    const initial = initialState();
    Object.keys(initial.sucess).forEach(key => {
      state[key] = initial.sucess[key];
    });
  },
  resetState(state) {
    helpers.resetState(state);
  }
};

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
};
