import CurrentUserApi from "../../backend/api/currentUser";

const initialState = () => ({
  data: {},
  signedIn: false,
  authToken: null
});

const getters = {};

const actions = {
  getCurrentUserDetails({ commit }) {
    return new Promise((resolve, reject) => {
      CurrentUserApi.getCurrentUserDetails()
        .then(response => {
          commit("setCurrentuserDetails", response.data);
          resolve();
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  updateCurrentUserDetails({ commit }, { params, success }) {
    return new Promise((resolve, reject) => {
      CurrentUserApi.updateCurrentUserDetails(params)
        .then(response => {
          commit("setCurrentuserDetails", response.data);
          // not working??
          commit("users/updateUser", response.data, { root: true });
          commit("notifications/setSuccess", success, { root: true });
          resolve();
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

const mutations = {
  setNavigationOpen(state, value) {
    state.navigation.open = value;
  },
  setCurrentUser(state, data) {
    state.signedIn = true;
    state.data = data;
    state.authToken = data.jwt;
  },
  unsetCurrentUser(state) {
    state.data = {};
    state.signedIn = false;
    state.authToken = null;
  },
  setCurrentuserDetails(state, data) {
    state.data = data;
  },
  resetState(state) {
    const initial = initialState();
    Object.keys(initial).forEach(key => {
      state[key] = initial[key];
    });
  }
};

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
};
