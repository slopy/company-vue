import Vue from "vue";
import App from "./App.vue";
import Vuetify from "vuetify";
import router from "./router";
import store from "./store";

import VueAxios from "vue-axios";

import { securedAxiosInstance, plainAxiosInstance } from "./backend/axios";

import "vuetify/dist/vuetify.min.css";
Vue.use(VueAxios, {
  secured: securedAxiosInstance,
  plain: plainAxiosInstance
});

Vue.use(Vuetify, {
  options: { customProperties: true }
});

new Vue({
  render: h => h(App),
  router,
  store,
  securedAxiosInstance,
  plainAxiosInstance
}).$mount("#app");

Vue.prototype.$httpPlain = plainAxiosInstance;
Vue.prototype.$httpSecure = securedAxiosInstance;
