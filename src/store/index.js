import Vue from "vue";
import Vuex from "vuex";

import { securedAxiosInstance, plainAxiosInstance } from "./../backend/axios";
import createPersistedState from "vuex-persistedstate";

import currentUser from "./modules/currentUser";
import users from "./modules/users";
import notifications from "./modules/notifications";
import navigation from "./modules/navigation";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    currentUser: currentUser,
    users: users,
    notifications: notifications,
    navigation: navigation
  },
  plugins: [createPersistedState()]
});

store.$httpPlain = plainAxiosInstance;
store.$httpSecure = securedAxiosInstance;

export default store;
