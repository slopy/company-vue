import UserSignIn from "./../../components/currentUser/UserSignIn.vue";
import UserSignUp from "./../../components/currentUser/UserSignUp.vue";
import CurrentUserShowProfile from "./../../components/currentUser/CurrentUserShowProfile.vue";
import CurrentUserEditProfile from "./../../components/currentUser/CurrentUserEditProfile.vue";

const currentUserRoutes = [
  {
    path: "/sign-in",
    name: "SignIn",
    component: UserSignIn
  },
  {
    path: "/sign-up",
    name: "SignUp",
    component: UserSignUp
  },
  {
    path: "/profile",
    name: "CurrentUserShowProfile",
    component: CurrentUserShowProfile
  },
  {
    path: "/edit-profile",
    name: "CurrentUserEditProfile",
    component: CurrentUserEditProfile
  }
];

export { currentUserRoutes };
