import { securedAxiosInstance } from "./../axios";

const createPaginationParamsObject = function(pagination) {
  let { page, rowsPerPage } = pagination;
  return { page: page, perPage: rowsPerPage };
};

const addHeaders = function(response) {
  response.pagination = {
    totalItems: parseInt(response.headers["x-total"])
  };
  return response;
};

export default {
  getAllUsers(pagination) {
    var paginationParams = createPaginationParamsObject(pagination);

    return securedAxiosInstance
      .get("/users/", { params: { ...paginationParams } })
      .then(response => {
        addHeaders(response);
        return response;
      });
  },
  updateUser(id, params) {
    return securedAxiosInstance.put("/users/" + id, params).then(response => {
      return response;
    });
  }
};
