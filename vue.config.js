module.exports = {
  devServer: {
    proxy: {
      "/api": {
        target: "127.0.0.1:3000/api",
        changeOrigin: true
      }
    }
  }
};
