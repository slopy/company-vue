import UsersApi from "../../backend/api/users";

const initialState = () => ({
  users: []
});

const getters = {};

const actions = {
  getAllUsers({ commit }, { pagination }) {
    return new Promise((resolve, reject) => {
      UsersApi.getAllUsers(pagination)
        .then(response => {
          commit("setUsers", response.data.data);
          resolve(response);
        })
        .catch(error => {
          commit("resetState");
          reject(error);
        });
    });
  },
  updateUser({ commit }, { id, params, success }) {
    return new Promise((resolve, reject) => {
      UsersApi.updateUser(id, params)
        .then(response => {
          commit("updateUser", response.data.data);
          commit("notifications/setSuccess", success, { root: true });
          resolve(response);
        })
        .catch(error => {
          commit("resetState");
          reject(error);
        });
    });
  }
};

const mutations = {
  setUsers(state, data) {
    state.users = data;
  },
  updateUser(state, data) {
    // do we need sort?
    state.users = [...state.users.filter(el => el.id !== data.id), data];
  },
  resetState(state) {
    const initial = initialState();
    Object.keys(initial).forEach(key => {
      state[key] = initial[key];
    });
  }
};

export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
};
